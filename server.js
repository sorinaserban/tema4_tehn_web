const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')

const mysql = require('mysql2/promise')

const DB_USERNAME = 'root'
const DB_PASSWORD = 'p@ss'

mysql.createConnection({
	user : DB_USERNAME,
	password : DB_PASSWORD
})
.then(async (connection) => {
	await connection.query('DROP DATABASE IF EXISTS tw_exam')
	await connection.query('CREATE DATABASE IF NOT EXISTS tw_exam')
})
.catch((err) => {
	console.warn(err.stack)
})

const sequelize = new Sequelize('tw_exam', DB_USERNAME, DB_PASSWORD,{
	dialect : 'mysql',
	logging: false,
	define: {
    	timestamps: false
	},
})

let Author = sequelize.define('author', {
	name : Sequelize.STRING,
	email : Sequelize.STRING,
	address : Sequelize.STRING,
	age : Sequelize.INTEGER
})

let Book = sequelize.define('book', {
	title : Sequelize.STRING,
	pages : Sequelize.INTEGER 
})

Author.hasMany(Book)

const app = express()
app.use(bodyParser.json())

app.get('/create', async (req, res) => {
	try{
		await sequelize.sync({force : true})
		for (let i = 0; i < 10; i++){
			let author = new Author({
				name : 'name ' + i,
				email : 'name' + i + '@nowhere.com',
				address : 'some address on ' + i + 'th street'
			})
			await author.save()
		}
		res.status(201).json({message : 'created'})
	}
	catch(err){
		console.warn(err.stack)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/authors', async (req, res) => {
	// should get all authors
	try{
		let authors = await Author.findAll()
		res.status(200).json(authors)
	}
	catch(err){
		console.warn(err.stack)
		res.status(500).json({message : 'server error'})		
	}
})

app.post('/authors', async (req, res) => {
	try{
		let author = new Author(req.body)
		await author.save()
		res.status(201).json({message : 'created'})
	}
	catch(err){
		// console.warn(err.stack)
		res.status(500).json({message : 'server error'})		
	}
})

app.post('/authors/:id/books', async (req, res) => {
	// TODO: implementați funcția
	// ar trebui să adauge o carte la un autor
	const book = req.body;
	const authorId = req.params.id;
	book.authorId = authorId;
	
	if(book.title && book.pages) {
		Author.findOne({ where: { id: authorId }}).then(result => {
			if(result) {
				Book.create(book).then(() => {
					res.status(201).send({message: 'created'});
				})
			}
			else {
				res.status(404).send({message: 'not found'});
			}
		});
	}
	// TODO: implement the function
	// should add a book to an author
})

app.get('/authors/:id/books', async (req, res) => {
	// TODO: implementați funcția
	// ar trebui să listeze toate cărțile unui autor
	const authorId = req.params.id;
	
	Author.findOne({where: {id: authorId} }).then(result => {
		if(result) {
			Book.findAll({where: { authorID: authorId} }).then(result => {
				res.status(200).send(result);
			});
		}
		else {
			res.status(404).send({message: 'not found'});
		}
	});
	// TODO: implement the function
	// should list all of an authors' books
})


app.listen(8080)

module.exports = app